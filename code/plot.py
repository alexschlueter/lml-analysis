import numpy as np

import matplotlib
from matplotlib import transforms
from matplotlib.lines import Line2D
import matplotlib.pyplot as plt
import matplotlib.cm as cm


matplotlib.rc('image', interpolation='nearest')
matplotlib.rc('figure',facecolor='white')
matplotlib.rc('image',cmap='viridis')


""" Plotting the results"""

def plotResult(rho, dataGT, fov, grid_sizes, timeSlices = [15, 40, 60]):

    nSlices = len(timeSlices)

    fig = plt.figure(figsize=(12, 4))
    for i, t in enumerate(timeSlices):
        fig.add_subplot(1, nSlices, i + 1)
        img = np.sum(rho[t], axis=2)
        img = np.minimum(1, 2 * img / np.max(img))
        # img=np.transpose(img)
        plt.imshow(img, origin="lower", cmap=cm.Blues)

        if dataGT is not None:
            ax = plt.gca()
            drawGT(ax, dataGT, t, grid_sizes, fov)
        # plt.axis('off')
    plt.tight_layout()
    plt.show()


def xyzToGrid(x, y, z, grid_sizes, fov):
    lin_x = np.linspace(fov.offset_x, fov.offset_x + fov.fov_x, grid_sizes[0] + 1)
    lin_y = np.linspace(fov.offset_y, fov.offset_y + fov.fov_y, grid_sizes[1] + 1)
    lin_z = np.linspace(fov.offset_z, fov.offset_z + fov.fov_z, grid_sizes[2] + 1)
    grid_point = []
    for i in range(grid_sizes[0]):
        if x <= lin_x[i]:
            grid_point += [i - 1]
            break

    for i in range(grid_sizes[1]):
        if y <= lin_y[i]:
            grid_point += [i - 1]
            break

    for i in range(grid_sizes[2]):
        if z <= lin_z[i]:
            grid_point += [i - 1]
            break
    return grid_point


def markPoint(ax, x, y):
    # ax.add_artist(plt.Circle((x, y), 1.5, color='r', fill=False, lw=1))
    s = 1.5
    ax.add_artist(plt.Line2D([x - s, x + s], [y - s, y + s], color='r', lw=1))
    ax.add_artist(plt.Line2D([x - s, x + s], [y + s, y - s], color='r', lw=1))


def drawGT(ax, dataGT, t, grid_sizes, fov):
    dataGTPlot = [dataGT[t, :, 0], dataGT[t, :, 1], dataGT[t, :, 2]]
    for x, y, z in zip(*dataGTPlot):
        tmp = xyzToGrid(x, y, z, grid_sizes, fov)
        markPoint(ax, tmp[1], tmp[0])

def drawEvents2D(ax, eventData, drawLines=False, drawDetections=False):
    assert eventData.shape[1] == 1 + 2 + 2 * 2
    ax.plot(eventData[:,1], eventData[:,2], 'rx')
    if drawLines:
        for event in eventData:
            ax.plot(*event[3:].reshape(2,-1).transpose(), 'r')
    if drawDetections:
        ax.plot(eventData[:,3], eventData[:,4], 'gx')
        ax.plot(eventData[:,5], eventData[:,6], 'gx')

def overlappingExtent(bbox, gridSizes):
    gridSizes = np.asarray(gridSizes)
    overlap = bbox.side_lengths() / (2 * (gridSizes - 1))
    return bbox.padded(overlap)

def drawGridSol(ax, weights, transform=transforms.IdentityTransform(), **kwargs):
    # extent = overlappingExtent(bbox, weights.shape).flatten()
    img = ax.imshow(weights.transpose(),
                    interpolation="none",
                    origin="lower",
                    aspect="auto",
                    extent=[0, 1, 0, 1],
                    transform=transform + ax.transData,
                    **kwargs)
    # img.set_extent([-3,1,0,1])

    bbox = transforms.Bbox.from_extents(0, 0, 1, 1)
    trbox = transforms.Bbox.null()
    trbox.update_from_data_xy(transform.transform(bbox.corners()))
    ax.set_xlim(trbox.x0, trbox.x1)
    ax.set_ylim(trbox.y0, trbox.y1)

    return img

def drawGrid(ax, bbox, grid_sizes):
    for dim, gs in enumerate(grid_sizes):
        for tick in range(gs + 1):
            pos = bbox.bounds[dim][0] + tick * bbox.side_lengths()[dim] / gs
            params = []
            for dim2 in range(len(grid_sizes)):
                if dim == dim2:
                    params.append([pos, pos])
                else:
                    params.append(bbox.bounds[dim2])
            ax.add_line(Line2D(*params))